// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/BoxComponent.h"
#include "PongGameCharacter.generated.h"

UCLASS(config=Game)
class APongGameCharacter : public ACharacter
{
	GENERATED_BODY()


public:
	APongGameCharacter();


protected:

	/** Handles moving forward/backward */
	void MoveForward(float Value);

	/** bool to track if character (pawn) was possessed*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player")
	bool isPossessed;

protected:

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


public:

};

