// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AdvancedFriendsGameInstance.h"
#include "PongGameAdvFriendsGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class PONGGAME_API UPongGameAdvFriendsGameInstance : public UAdvancedFriendsGameInstance
{
	GENERATED_BODY()
	
};
