// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PongGamePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class PONGGAME_API APongGamePlayerController : public APlayerController
{
	GENERATED_BODY()
	
};
