// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PongGameGameMode.generated.h"

UCLASS(minimalapi)
class APongGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APongGameGameMode();

	/** bool to track if the game has started */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rules")
	bool bGameStarted;

	/** Game win condition. Once a player reached this number, game will end*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rules")
	int32 WinningScore;
	
};



