// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "PongGameProjectile.generated.h"

UCLASS()
class PONGGAME_API APongGameProjectile : public AActor
{
	GENERATED_BODY()
	
public:	

	APongGameProjectile();

protected:

	virtual void BeginPlay() override;

	/** Direction vector where the ball should move*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FVector TargetDirection;

	/** Base speed value for this projectile. Other scaling may affect final rate. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float ProjectileSpeed;

	/** Projectile's movement component */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
	UProjectileMovementComponent* BallProjectileMovementComponent;

	/** Int to track current level of projectile "intensity". Used for visual cue of current ball speed */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
	int32 BallSpeedLevel;
	

	/** Projectile's mesh */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ball")
	UStaticMeshComponent* BallStaticMesh;

	/** Projectile's collision (Box) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ball")
	UBoxComponent* BallBoxComponent;

	
	/** 
	* Change velocity for both server and client(s) 	
	* @param Velocity	Velocity vector to set for this projectile.
	*/
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Movement")
	void Multi_SetBallVelocity(FVector Velocity);

	/** 
	* Change speed for both server and client(s) 
	* @param Speed	Base speed value to set for this projectile. Other scaling may affect final rate.
	*/
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Movement")
	void Multi_SetBallSpeed(float Speed);
	 
	/** 
	* Calculates a random "horizontal-like" vector with given offset 
	* @param ForwardVector	Direction vector where the ball should move 
	* @param DeviationValue	Offset value; Randomize the vector based on this value
	*/
	UFUNCTION(BlueprintPure, Category = "CustomVectorMath")
	FVector CalcRandomHorizonalValueInRange(FVector ForwardVector, float DeviationValue); // used to introduce direction randomness on "perfect hit"

};
