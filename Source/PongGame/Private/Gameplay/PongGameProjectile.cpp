// Fill out your copyright notice in the Description page of Project Settings.


#include "Gameplay/PongGameProjectile.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/Vector.h"
#include "Math/Quat.h"
#include "Core/PongGameGameMode.h"


APongGameProjectile::APongGameProjectile()
{
	// create
	BallStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	BallBoxComponent = CreateDefaultSubobject<UBoxComponent>("BoxComponent");
	BallProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>("ProjectileMovementComponent");

	// setup up attachments
	RootComponent = BallBoxComponent;
	BallStaticMesh->SetupAttachment(RootComponent);

	// init starting speed
	ProjectileSpeed = 1500;

	// init projectile movement component
	BallProjectileMovementComponent->ProjectileGravityScale = 0;
	BallProjectileMovementComponent->bInterpMovement = true;
	BallProjectileMovementComponent->InterpLocationTime = 0.2;
}

void APongGameProjectile::BeginPlay()
{
	Super::BeginPlay();

	if (ROLE_Authority) {
		
		// calculate random vector within these ranges
		int randomval = rand() % 2;
		TargetDirection.X = FMath::RandRange(-500, 500);
		TargetDirection.Z = 0;
		
		if (randomval == 0) {
			TargetDirection.Y = FMath::RandRange(-500, -450);
		} else {
			TargetDirection.Y = FMath::RandRange(450, 500);
		}

		// update projectile movement's velocity value
		TargetDirection.Normalize();
		Multi_SetBallVelocity(TargetDirection * ProjectileSpeed);
	}
}

void APongGameProjectile::Multi_SetBallVelocity_Implementation(FVector Velocity) {
	BallProjectileMovementComponent->Velocity = Velocity;
}

void APongGameProjectile::Multi_SetBallSpeed_Implementation(float Speed) {
	ProjectileSpeed = Speed;
}

FVector APongGameProjectile::CalcRandomHorizonalValueInRange(FVector ForwardVector, float DeviationValue) {

	ForwardVector.Normalize();
	FVector result;

	// set vector "offsets"
	result.X = FMath::FRandRange ((ForwardVector.X + DeviationValue), (ForwardVector.X - DeviationValue));
	result.Y = FMath::FRandRange((ForwardVector.Y + DeviationValue), (ForwardVector.Y - DeviationValue));
	result.Z = 0;

	// return normalized
	result.Normalize();
	return result * ProjectileSpeed;
}


