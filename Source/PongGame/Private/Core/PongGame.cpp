// Copyright Epic Games, Inc. All Rights Reserved.

#include "Core/PongGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, PongGame, "PongGame" );
 