// Copyright Epic Games, Inc. All Rights Reserved.

#include "Core/PongGameGameMode.h"
#include "Core/PongGameCharacter.h"
#include "UObject/ConstructorHelpers.h"

APongGameGameMode::APongGameGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default win score to 3
	WinningScore = 3;
}
